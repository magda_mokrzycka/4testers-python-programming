from cars import get_country_of_a_car_brand


def test_get_country_for_a_japanese_car():
    assert get_country_of_a_car_brand("Toyota") == "Japan"


def test_get_country_for_a_japanese_car_capitalized():
    assert get_country_of_a_car_brand("toyota") == "Japan"

def test_get_country_for_a_german_car():
    assert get_country_of_a_car_brand("Mercedes") == "Germany"

def test_get_country_for_a_german_car_lowercase():
    assert get_country_of_a_car_brand("mercedes") == "Germany"
