def reformat_temperatures_from_celsius_to_fahrenheit(abc):
    # for x in abc:
    #     return (x * 9 / 5) + 32
    new_list = []
    for x in abc:
        y = (x * 9 / 5) + 32
        new_list.append(y)
    return new_list


temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]

if __name__ == '__main__':
    print(reformat_temperatures_from_celsius_to_fahrenheit(temps_celsius))

#by Adrian

def convert_celsius_to_fahrenheit(temperatures_in_celsius):
    temps_fahrenheit = []
    for temperature in temperatures_in_celsius:
        temps_fahrenheit.append(round(temperature * 9 / 5 + 32, 2))
    return temps_fahrenheit


if __name__ == '__main__':
    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print(convert_celsius_to_fahrenheit(temps_celsius))
    convert_celsius_to_fahrenheit(100)
