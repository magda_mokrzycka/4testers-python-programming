def get_country_of_a_car_brand(car_brand):
    car_brand_lowercase = car_brand.lower()
    if car_brand_lowercase in ("toyota", "mazda", "suzuki", "subaru"):
        return "Japan"
    elif car_brand_lowercase in ("bMW", "mercedes", "audi", "volkswagen"):
        return "Germany"
    elif car_brand_lowercase in ("renault", "peugeot"):
        return "France"
    else:
        return "Unknown"


