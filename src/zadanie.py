import random
import datetime

lists_of_dictionaries = []

# Listy zawierające dane do losowania
female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']


# Przykład jak można losować imię z listy
# random_female_firstname = random.choice(female_fnames)
# print(random_female_firstname)

# Przykład jak można losować wiek z liczb całkowitych od 1 do 65
# random_age = random.randint(1, 65)
# print(random_age)

# Przykładowy wygenerowany pojedynczy słownik
# example_dictionary = {
#     'firstname': 'Kate',
#     'lastname': 'Yu',
#     'email': 'kate.yu@example.com',
#     'age': 23,
#     'country': 'Poland',
#     'adult': True
# }


# 1. stworzyć pętlę for
def generate_firstname(number):
    if number < 5:
        return random.choice(female_fnames)
    else:
        return random.choice(male_fnames)


def generate_lastname():
    return random.choice(surnames)


def generate_country():
    return random.choice(countries)


def generate_age():
    return random.randint(5, 45)


def generate_adult(age):
    if age >= 18:
        return True
    else:
        return False


def get_current_year():
    today = datetime.date.today()
    year = today.strftime("%Y")
    return year


def generate_birth_year(age, year):
    return int(year) - age


def generate_one_dictionary(index):
    firstname = generate_firstname(index)
    lastname = generate_lastname()
    country = generate_country()
    email = f"{firstname.lower()}.{lastname.lower()}@example.com"
    age = generate_age()
    adult = generate_adult(age)
    birth_year = generate_birth_year(age, get_current_year())

    dict = {
        "firstname": firstname,
        "lastname": lastname,
        "country": country,
        "email": email,
        "age": age,
        "adult": adult,
        "birth_year": birth_year
    }

    return dict


def generate_dictionaries():
    for i in range(0, 10):
        dictionary = generate_one_dictionary(i)
        lists_of_dictionaries.append(dictionary)

    for i in range(0, 10):
        firstname = lists_of_dictionaries[i]['firstname']
        lastname = lists_of_dictionaries[i]['lastname']
        country = lists_of_dictionaries[i]['country']
        birth_year = lists_of_dictionaries[i]['birth_year']
        print(f"Hi! I'm {firstname} {lastname}. I come from {country} and I was born in {birth_year}")


generate_dictionaries()

# 2 wewnatrz petli nazwac funkcje do generowania jednego slownika (nie towrzy jeszcze) ktora przyhmie indeks z petli for
# 3 stworzyc funckje jw ktora przyjmie indeks z petli
# 4 wewnatrz jw funkcji rozpoczac tworzenie slownika, klucz niech juz bedzie utworzony, wartoscia bedzie funkcja do generowania dnego klucza
# 5 nazwac funkcje do generowania danych w miejscu "wartosc" (tj. klucz, wartosc np. "name", generujImie(i)
# 5.5 stworztyc funkcje do generowania per klucz
# 6 utworzony slownik dodac do listy (nie jest jeszcze stworzona, stworzyc liste powyzej petli for)
# 7 nic
# 8 wyjsc z petli, za petla, ponizej, zrobic druga petle z takim samym zakresem
# 9 wewnarz tej nowo utworzonej petli wyprintowac poszczegolne slowniki, pewnie print(slownik[i])
