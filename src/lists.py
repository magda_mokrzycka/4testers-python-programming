#Define 5 favourites movies as a list
movies = ['Cast Away', 'The Last of Us', 'The Office', 'Menu', 'Moonfall']
print(movies)
first_movie = movies[0]
print(f"First movie is {first_movie}.")

last_movie = movies[-1] #ostatni element indeksujemy jako -1
print(f"Last movie is {last_movie}.")

movies.append("Dune") #dodawanie elementu do listy
print(f"The length of movies list is {len(movies)}")

movies.insert(0, "Star Wars")
movies.remove("The Last of Us")
print(movies)

movies[-1] = "Dune (1980)" #zastąpienie ostatniego elementu innym
print(movies)

#Excersize - list of emails
emails = ["a@example.com", "b@example.com"]
print(f"The length of emails list is {len(emails)}")
first_email = emails[0]
print(f"First email is {first_email}.")

last_email = emails[-1]
print(f"The last email is {last_email}.")

emails.append("cde@example.com")
print(emails)