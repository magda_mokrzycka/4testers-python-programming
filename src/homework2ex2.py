import random
import string


def get_random_string(p1, p2, p3, p4, p5):
    letters = string.ascii_lowercase
    p1 = ''.join(random.choice(letters) for i in range(p1))
    p2 = ''.join(random.choice(letters) for i in range(p2))
    p3 = ''.join(random.choice(letters) for i in range(p3))
    p4 = ''.join(random.choice(letters) for i in range(p4))
    p5 = ''.join(random.choice(letters) for i in range(p5))
    return p1 + "-" + p2 + "-" + p3 + "-" + p4 + "-" + p5


def random_log_data(parametr1):
    randomPassword = get_random_string(8,4,4,4,12)
    user = {
        "email": parametr1,
        "password": randomPassword,
    }
    print(user)


# ---------------------------------------------

# ---------------------------------------------

random_log_data("magda@wp.pl")
