#Excersize 1 Print welcome msg to a person in a city

def print_hello_message(name, city):
    name_capitalized = name.capitalize()
    city_capitalized = city.capitalize()
    print(f"Witaj { name_capitalized }! Miło Cię widzieć w naszym mieście: { city_capitalized }!")

print_hello_message("Michał", "Toruń")
print_hello_message("Beata", "Gdynia")
print_hello_message("adam", "warszawa")

#Excersize 2 Generate 4testers.pl domain using first and last name

def get_email(name, surname):
    return f"{name.lower()}.{surname.lower()}@4testers.pl"
print(get_email("Janusz", "Nowak"))
print(get_email("Barbara", "Kowalska"))


