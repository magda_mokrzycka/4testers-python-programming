#LISTS
shopping_list = ['oranges', 'water', 'chicken', 'potatoes', 'bananas']
print(shopping_list[0])
print(shopping_list[2])
print(shopping_list[-1]) #ostatni element -2 oznacza przedostatni element listy
shopping_list.append('lemons') #dodawanie elementów do listy
print(shopping_list)

number_of_items_to_buy = len(shopping_list)
print(number_of_items_to_buy) #liczba elementów listy

first_three_shopping_items = shopping_list[0:3]
print(first_three_shopping_items)

#Define 5 favourites movies as a list
movies = ['Cast Away', 'The Last of Us', 'The Office', 'Menu', 'Moonfall']
print(movies)
first_movie = movies[0]
print(f"First movie is {first_movie}.")

last_movie = movies[-1] #ostatni element indeksujemy jako -1
print(f"Last movie is {last_movie}.")

movies.append("Dune") #dodawanie elementu do listy
print(f"The length of movies list is {len(movies)}")

movies.insert(0, "Star Wars")
movies.remove("The Last of Us")
print(movies)

movies[-1] = "Dune (1980)" #zastąpienie ostatniego elementu innym
print(movies)

#Excersize - list of emails
emails = ["a@example.com", "b@example.com"]
print(f"The length of emails list is {len(emails)}")
first_email = emails[0]
print(f"First email is {first_email}.")

last_email = emails[-1]
print(f"The last email is {last_email}.")

emails.append("cde@example.com")
print(emails)

#DICTONARIES
animal = {
    "name": "Fibi",
    "kind": "dog",
    "age": 7,
    "male": False
}
dog_age = animal["age"]
print("Dog age:", dog_age)
dog_name = animal ["name"]
print("Dog name:", dog_name)

animal["age"] = 10
print(animal)
animal["owner"] = "Magda"
print(animal)

#Excersize
friend = {
    "name" : "Monika",
    "age" : "32",
    "hobby": ["swimming", "singing"]
}
print(friend)
friend["city"] = "Wroclaw"
friend["age"] = 34
print(friend)
friend["job"] = "teacher"
del friend["job"]
print(friend["hobby"][-1])

