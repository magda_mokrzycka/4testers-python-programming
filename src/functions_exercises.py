def upper_word(word):
    return word.upper()

big_dog = upper_word("dog")
print(big_dog)
print(upper_word("tree"))

def print_a_car_brand_name():
    print("Honda")

def print_given_number_multiplied_by_3(input_number):
    print(input_number * 3)

#jeżeli funkcja coś printuje jest nieprzydatna dalej w kodzie, printowanie pokazuje coś tylko nam, a dla programu powinniśmy
#używać return
def calculate_area_of_a_circle(radius):
    return 3.1415 * radius ** 2

def calculate_area_of_a_triangle(bottom, height):
    return 0.5 * bottom * height

print_a_car_brand_name()
print_given_number_multiplied_by_3(40)
area_of_a_circle_with_radius_10 = calculate_area_of_a_circle(10)
print(area_of_a_circle_with_radius_10)
area_of_a_little_triangle = calculate_area_of_a_triangle(5, 5)
print(area_of_a_little_triangle)

#Checking what is returned from a function with default
result_of_a_print_function = print_a_car_brand_name()
print(result_of_a_print_function)
