import uuid

def print_10_random_uuids():
    for i in range(10):
        print(str(uuid.uuid4()))

def print_numbers_from_20_to_30():
    for number in range(20, 31):
        print(number)


if __name__ == '__main__':
    print_10_random_uuids()
    print_numbers_from_20_to_30