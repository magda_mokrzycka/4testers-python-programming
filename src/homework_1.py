#Excersize 1
def get_number_squared(c):
    return c ** 2

#zmienne np c tutaj są widoczne tylko w obrębie funkcji

zero_squared = get_number_squared(0)
print(zero_squared)

sixteen_squared = get_number_squared(16)
print(sixteen_squared)

floating_number_squared = get_number_squared(2.55)
print(floating_number_squared)

#Excersize 2
def get_cuboid_volume(a, b, h):
    return a * b * h

print(get_cuboid_volume(3, 5, 7))

#Excersize 3
# conversion of degree
# F = (C * 9/5) + 32
# C = (F-32) * 5/9

def convert_celcius_degrees_to_fahrenheit(celcius_degree):
    return celcius_degree * 9 / 5 + 32

fahrenheit = convert_celcius_degrees_to_fahrenheit(20)
print(fahrenheit)




