countries_map = [
    {'name': 'Japan', 'population': 50_000_000, 'continent': 'Asia'},
    {'name': 'South Korea', 'population': 20_000_000, 'continent': 'Asia'},
    {'name': 'Germany', 'population': 60_000_000, 'continent': 'Europe'},
]
