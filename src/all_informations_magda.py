#F-string
first_name = "Magdalena"
last_name = "Mokrzycka"
email = "magdakaczorek1991@gmail.com"
my_bio = f"Mam na imię {first_name}. Na nazwisko mam {last_name}. \nMój mail to: {email}."
print(my_bio)

print(f"Wynik operacji mnożenia 4 przez 4 wynosi {4 * 4}.")

#\n # -> daje nową linię
#sep = "" -> zmienia separator w stringach
# 2 ** 2 -> to oznacza 2 do potęgi drugiej w obliczeniach matematycznych potęga to **
# cmnd d -> duplikuje linijkę
# cntrl alt l -> Pycharm formatuje kod

### FUNKCJE ###
# def + nazwa funkcji ():


def calculate_area_of_a_circle(radius):
    return 3,14 * radius ** 2 # -> ta funkcja niczego nie printuje


area_of_a_circle_with_radius_4 = calculate_area_of_a_circle(4) #-> dlatego została przypisana do zmiennej,
                                                                    #żeby wyprintować wynik
print(area_of_a_circle_with_radius_4)

#loops pętle
# for i in range(10):
    #blok wykonany 10 razy

#można stworzyć z danymi plik np o nazwie config
# aby z niego zaimportowac wpisujemy import.config






