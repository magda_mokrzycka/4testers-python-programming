my_name = "Magda"
my_age = 32

print(my_name)
print(my_age)

# Below is the description of my friend
bf_name = "Monika"
bf_age = 32
bf_pets = 0
has_driving_license = True
years_of_friendship = 20

print(bf_name)
print(bf_age)
print(bf_pets)
print(has_driving_license)
print(years_of_friendship)